export const environment = {
  production: true,
  basePokemonApiUrl: 'https://pokeapi.co/api/v2/',
  baseDatabaseApiUrl: 'http://localhost:5000/'
};
