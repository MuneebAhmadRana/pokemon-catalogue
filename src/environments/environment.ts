export const environment = {
  production: false,
  basePokemonApiUrl: 'https://pokeapi.co/api/v2/',
  baseDatabaseApiUrl: 'http://localhost:5000/'
};
