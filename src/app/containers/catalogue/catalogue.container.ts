import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator/paginator';
import { Router } from '@angular/router';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.container.html',
  styleUrls: ['./catalogue.container.css'],
})
export class CatalogueContainer implements OnInit {
  paginatorIndex: number = 0;
  pokemonCount: number = 0;
  getIdAndImage (url: string): { id: string; url: string } {
    const id = url.split('/').filter(Boolean).pop();
    return {
      id,
      url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
    };
  }

  onClicked (pokemonName: number): void {
    console.log('Redirecting to ', pokemonName);
    /* this.router.navigate(['pokemon', id]); */
  }

  get pokemonList$ () {
    this.pokemonCount = this.pokemonList.getCount();
    return this.pokemonList.getPokemonList();
  }

  onPaginatorChanged (event: PageEvent) {
    const { pageIndex } = event;
    if (pageIndex > this.paginatorIndex) {
      this.paginatorIndex = pageIndex;
      this.pokemonList.nextPokemonPage();
    } else {
      this.paginatorIndex = pageIndex;
      this.pokemonList.previousPokemonPage();
    }
    window.scroll(0, 0);
  }

  constructor(
    private router: Router,
    private pokemonList: PokemonCatalogueService
  ) { }

  ngOnInit (): void { }
}
