import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-landing-page',
  styleUrls: ['./landing-page.container.css'],
  templateUrl: './landing-page.container.html',
})
export class LandingPageContainer implements OnInit {
  isLoading = false;
  userFormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.minLength(2),
      Validators.required,
    ]),
  });

  constructor(private router: Router, private userService: UserService) {}

  onLoginClicked() {
    this.isLoading = true;
    try {
        this.userService.fetchUser(this.username.value);
    } catch (e) {
        this.isLoading = false;
        alert(e.message);
    }
  }

  get username(): AbstractControl {
    return this.userFormGroup.get('username');
  }

  get user(): Observable<User> {
    return this.userService.getUser();
  }

  getError() {
    if (this.username.hasError('required')) {
      return 'You must enter you trainer name';
    } else if (this.username.hasError('minlength')) {
      return 'Must be longer than 1';
    } else return '';
  }

  ngOnInit() {
    this.user.subscribe(user => {
        if (user) {
            this.router.navigateByUrl('/trainer-page');
        } else {
            if (this.isLoading) {
                this.isLoading = false;
                alert('Could not lot in');
            }
        }
    });
  }
}
