import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: "pokemon-detail",
    templateUrl: "./pokemon-detail.container.html",
    styleUrls: ["./pokemon-detail.container.css"]
})
export class PokemonDetailContainer implements OnInit {

    @Input() pokemonName: string;

    ngOnInit () {

    }
}