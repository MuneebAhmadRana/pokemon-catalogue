import { Component, OnInit } from "@angular/core";
import { TrainerPageService } from 'src/app/services/trainer-page.service';
import { Observable } from 'rxjs';
import { PokemonListItem } from 'src/app/models/pokemon-list.model';

@Component({
    selector: "trainer-page",
    templateUrl: "./trainer-page.container.html",
    styleUrls: ["./trainer-page.container.css"]
})
export class TrainerPageContainer implements OnInit {
    public title: string = "Welcome to Trainer Page"
    constructor(
        private trainerPageService: TrainerPageService
    ) { }

    get trainersPokemons$ (): Observable<PokemonListItem[]> {
        return this.trainerPageService.trainersPokemons$()
    }

    onPokemonClicked (pokemonName) {

    }

    ngOnInit (): void {
        this.trainerPageService.fetchNameAndImage()
    }
}