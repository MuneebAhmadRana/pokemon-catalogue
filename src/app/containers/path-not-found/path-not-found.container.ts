import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-path-not-found',
  templateUrl: './path-not-found.container.html',
  styleUrls: ['./path-not-found.container.css']
})
export class PathNotFoundContainer implements OnInit {

  constructor() { }

  ngOnInit (): void {
  }

}
