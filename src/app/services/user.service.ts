import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _user: BehaviorSubject<User> = new BehaviorSubject(null);
  baseBatabaseApiUrl = environment.baseDatabaseApiUrl;

  
  constructor(private readonly http: HttpClient) {}

  getUser(): Observable<User> {
    return this._user.asObservable();
  }

  fetchUser(name: string): void {
    this.http
      .get<User[]>(`${this.baseBatabaseApiUrl}users/?name=${name}`)
      .subscribe((user: User[]) => {
        this._user.next(user.pop());
      });
  }

  isLoggetIn(): boolean {
    return this._user.value.id ? true : false;
  }
}
