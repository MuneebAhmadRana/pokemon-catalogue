import { Injectable } from "@angular/core";

@Injectable()
export class LocalstorageService {
    constructor(){}

    setUserToStorage(): void {
        // TODO: Use the User Service to get the current user and save it to the local storage.
        localStorage.setItem('user', "Bruh");
    }

    getUserFromStorage(): string {
        // TODO: Get the user from the local storage. Use a model instead of string.
        return localStorage.getItem('user');
    }
}