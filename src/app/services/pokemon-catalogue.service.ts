import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonList, PokemonListItem } from '../models/pokemon-list.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private _pokemonList$: BehaviorSubject<
    PokemonListItem[]
  > = new BehaviorSubject([]);
  baseUrl = environment.basePokemonApiUrl;
  prev: string = null;
  next: string = null;
  count: number = 0;

  constructor(private readonly http: HttpClient) {
    this.fetchPokemonList();
  }

  getPokemonList(): Observable<any> {
    return this._pokemonList$.asObservable();
  }

  fetchPokemonList(): void {
    this.http
      .get<PokemonList>(`${this.baseUrl}pokemon/?limit=50`)
      .subscribe((data) => {
        this.prev = data.previous;
        this.next = data.next;
        this._pokemonList$.next(data.results);
        this.count = data.count;
      });
  }

  nextPokemonPage(): void {
    this.http.get<PokemonList>(this.next).subscribe((data) => {
      this.prev = data.previous;
      this.next = data.next;
      this._pokemonList$.next(data.results);
    });
  }

  previousPokemonPage(): void {
    this.http.get<PokemonList>(this.prev).subscribe((data) => {
      this.prev = data.previous;
      this.next = data.next;
      this._pokemonList$.next(data.results);
    });
  }

  resetPokemonList(): void {
    this.fetchPokemonList();
  }

  getCount(): number {
    return this.count;
  }
}
