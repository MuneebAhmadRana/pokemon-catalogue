import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { PokemonListItem, PokemonTrainer } from "../models/pokemon-list.model"
import { environment } from "../../environments/environment";
import { BehaviorSubject, Observable } from 'rxjs';
import { capitalize } from '../utils/capitalize.util';

const { basePokemonApiUrl } = environment;

@Injectable({
    providedIn: "root"
})
export class TrainerPageService {

    constructor(private readonly http: HttpClient) {

    }


    private readonly _trainersPokemons$: BehaviorSubject<PokemonListItem[]> = new BehaviorSubject([]);
    private trainerPokemonsList: PokemonListItem[] = []
    public trainer: PokemonTrainer = {
        id: 1,
        name: "Andreas",
        caughtPokemon: [1, 2, 3, 4, 5, 6, 7, 8, 9]
    }

    private getIdAndImage (id: number): any {
        return {
            id,
            url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
        };
    }

    trainersPokemons$ (): Observable<PokemonListItem[]> {
        return this._trainersPokemons$.asObservable()
    }

    fetchNameAndImage (): void {
        this.trainer.caughtPokemon.forEach(id => {
            let pokemonListItem: PokemonListItem = {
                id: id,
                name: null,
                url: null
            }
            const returnObject = this.getIdAndImage(id);
            pokemonListItem.url = returnObject.url;
            this.http.get(`${basePokemonApiUrl}pokemon/${id}`)
                .subscribe(({ name }: any) => {
                    pokemonListItem.name = capitalize(name);
                })
            this.trainerPokemonsList.push(pokemonListItem);
        })
        this._trainersPokemons$.next(this.trainerPokemonsList);
    }

    /*  */

}