export interface User {
    id?: number;
    trainerName: string;
    caughtPokemon: Array<number>;
}