import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageContainer } from './containers/landing-page/landing-page.container';
import { CatalogueContainer } from './containers/catalogue/catalogue.container';
import { TrainerPageContainer } from './containers/trainer-page/trainer-page.container';
import { PokemonDetailContainer } from "./containers/pokemon-detail/pokemon-detail.container"
import { PathNotFoundContainer } from './containers/path-not-found/path-not-found.container';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
  },
  {
    path: 'login',
    component: LandingPageContainer,
  },
  {
    path: 'catalogue',
    component: CatalogueContainer,
  }, {
    path: "trainer-page",
    component: TrainerPageContainer
  },
  {
    path: "pokemon-detail",
    component: PokemonDetailContainer
  }, {
    path: "**",
    component: PathNotFoundContainer
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
