import { PokemonList } from "../models/pokemon-list.model";
import { Pokemon } from "../models/pokemon.model";

// Converts JSON strings to/from your types
export class Convert {
    public static toPokemonList(json: string): PokemonList {
        return JSON.parse(json);
    }

    public static pokemonListToJson(value: PokemonList): string {
        return JSON.stringify(value);
    }

    public static toPokemon(json: string): Pokemon {
        return JSON.parse(json);
    }

    public static pokemonToJson(value: Pokemon): string {
        return JSON.stringify(value);
    }
}